package org.example.threading.snip2_Sleep;

public class FirstThread extends Thread {
    public Object start;

    @Override
    public void run() {
        System.out.println(" A very Long Operation ");
        int sum = 0;

        for (int i = 0; i < 1000000; i++) {
            sum +=i;

            if (i % 10000 == 0) {
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("result = " + sum);

        System.out.println(" Finished ");
    }
}
