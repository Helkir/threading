package org.example.threading.wordstat;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MainStreams {
    public static void main(String[] args) {
        System.out.println("Enter Main");


        List<String> words = new ArrayList<>();

        words.add("The");
        words.add("and");
        words.add("before");
        words.add("after");
        words.add("them");

        int i = Runtime.getRuntime().availableProcessors(); // 16 cuz i9 hyperThread


        File dir = new File("books/");

        File[] files = dir.listFiles();
        WordStat stat = new WordStat();
        Arrays.stream(files)
                .parallel()
                .forEach(file -> {
                    words.parallelStream()
                            .forEach(word -> stat.searchWord(word, file));
                });

        System.out.println("Exit Main");

    }
}
