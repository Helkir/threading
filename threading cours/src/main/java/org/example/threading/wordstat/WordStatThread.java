package org.example.threading.wordstat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WordStatThread extends Thread {

    @Override
    public void run() {
        System.out.println("Enter run");
        List<String> words = new ArrayList<>();

        words.add("The");
        words.add("and");
        words.add("before");
        words.add("after");
        words.add("them");

        long start = System.currentTimeMillis();
        File dir = new File("books/");

        for(File file: dir.listFiles()) {

            System.out.println("------ File = " + file.getName() + " ------");
            for (String word : words) {
                searchWord(word, file);
            }
        }

        long elapsed = System.currentTimeMillis() - start;
        System.out.println("Elapsed : " + elapsed + "ms");
        System.out.println("Exit run");
    }
    private void searchWord(String word, File file) {
        int count = 0;
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(file));

            String line = reader.readLine();
            while (line != null) {
                count += occurenceIsOfWord(word, line);
                line = reader.readLine();
            }

            reader.close();
            System.out.println("word = "+ word + " count = "+ count );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int occurenceIsOfWord(String word, String text) {

        String cleanWord = word.toLowerCase();
        String cleanText = text.toLowerCase();
        int wordLenght = cleanWord.length();

        int count = 0;
        int index = 0;

        while (index != -1) {
            index = cleanText.indexOf(cleanWord, index + wordLenght);
            if (index != -1) {
                count++;
            }
        }

        return count;
    }
}
