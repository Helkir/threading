package org.example.threading.wordstat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class WordStat {
    public int searchWord(String word, File file) {
        int count = 0;
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(file));

            String line = reader.readLine();
            while (line != null) {
                count += occurenceIsOfWord(word, line);
                line = reader.readLine();
            }

            reader.close();
           // System.out.println("word = "+ word + " count = "+ count );
        } catch (IOException e) {
            e.printStackTrace();
        }
        return count;
    }

    public int occurenceIsOfWord(String word, String text) {

        String cleanWord = word.toLowerCase();
        String cleanText = text.toLowerCase();
        int wordLenght = cleanWord.length();

        int count = 0;
        int index = -wordLenght;

        while (index != -1) {
            index = cleanText.indexOf(cleanWord, index + wordLenght);
            if (index != -1) {
                count++;
            }
        }

        return count;
    }
}
