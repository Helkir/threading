package org.example.threading.wordstat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WordStatTask implements Runnable {

    List<String> words;
    File file;
    private final int delay;
    private volatile boolean isRunning;
    private GlobalStats globalStats;

    public WordStatTask(List<String> words, File file, int delay, GlobalStats globalStats) {
        this.words = words;
        this.file = file;
        this.delay = delay;
        this.globalStats = globalStats;

    }

    @Override
    public void run() {
        System.out.println("Enter run");
        isRunning = true;

        WordStat stat = new WordStat();

        for (String word : this.words) {
            if (isRunning == false) {
                break;
            }
            int count = stat.searchWord(word, file);
            globalStats.increaseWordCount(word,count);

            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        isRunning = false;
        System.out.println("Exit run");
    }

    public void stopThread() {
        isRunning = false;
    }

}
