package org.example.threading.wordstat;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class GlobalStats {

    private Map<String, Integer> wordsCount = new HashMap<>();

    public void generateStats(List<String> words ,List<File> files) {

        wordsCount.clear();
        for (String word : words) {
            wordsCount.put(word,0);
        }

        ExecutorService threadPool = Executors.newFixedThreadPool(16);
        List<WordStatTask> tasks = new ArrayList<>();

        long start = System.currentTimeMillis();

        for(File file: files) {

            System.out.println("------ File = " + file.getName() + " ------");

            int delay = 0;
            WordStatTask task = new WordStatTask(words,file,delay,this);
            threadPool.execute(task);
        }
        threadPool.shutdown();
        try {
            threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long elapsed = System.currentTimeMillis() - start;
        System.out.println("Elapsed : " + elapsed + "ms");
        System.out.println("Exit Main");

    }

    public void increaseWordCount(String word, int count) {

        int oldCount = wordsCount.get(word);
        wordsCount.put(word, oldCount + count);
    }

    @Override
    public String toString() {
        return "GlobalStats{" +
                "wordsCount=" + wordsCount +
                '}';
    }
}
