package org.example.threading.snippets.snip01_firstthread;

public class FirstThread extends Thread {
    @Override
    public void run() {

        System.out.println("Run Start");

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Run End");
    }
}
