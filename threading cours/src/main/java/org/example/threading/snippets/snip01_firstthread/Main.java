package org.example.threading.snippets.snip01_firstthread;

public class Main {
    public static void main(String[] args) {
        System.out.println("Enter Main");

        FirstThread thread = new FirstThread();
        thread.start();

        System.out.println("Exit Main");
    }
}
