import org.example.threading.wordstat.WordStat;
import org.junit.Assert;
import org.junit.Test;

public class WordStatTests {

    @Test
    public void countWord_sucess() {
        WordStat stat = new WordStat();
        String word = "the";
        String text = "The the tHe";

        Assert.assertEquals(3,
                stat.occurenceIsOfWord(word,text));
    }
}
